/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Junling Bu <linlinjavaer@gmail.com>
 */

#include "ns3/node.h"
#include "ns3/packet.h"
#include "ns3/simulator.h"
#include "ns3/node-container.h"
#include "ns3/net-device-container.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/seq-ts-header.h"
#include "ns3/wave-net-device.h"
#include "ns3/wave-mac-helper.h"
#include "ns3/wave-helper.h"


#include <fstream>
#include <vector>
#include <string>

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>


using namespace ns3;
/// Global Variables ////

/**
 * This simulation is to show the routing service of WaveNetDevice described in IEEE 09.4.
 *
 * note: although txPowerLevel is supported now, the "TxPowerLevels"
 * attribute of YansWifiPhy is 1 which means phy devices only support 1
 * levels. Thus, if users want to control txPowerLevel, they should set
 * these attributes of YansWifiPhy by themselves..
 */
class WaveNetDeviceExample
{
public:
  void SendWsmpExample (void);
  void SendIpExample (void);
  void SendWsaExample (void);
  int packetReceived;

private:
  void SendOneWsmpPacket (uint32_t channel, uint32_t seq);
  void SendIpPacket (uint32_t seq, bool ipv6);
  bool Receive (Ptr<NetDevice> dev, Ptr<const Packet> pkt, uint16_t mode, const Address &sender);
  bool ReceiveVsa (Ptr<const Packet>,const Address &, uint32_t, uint32_t);
  void CreateWaveNodes (void);

  NodeContainer nodes;
  NetDeviceContainer devices;
};

/**
* setup the wave nodes properties.
* 1. create nodes.
* 2. install mobility.
* 3. install mac,phy layers in nodes.
* 4. create the call back funcitons.
* 5. setup trace.
**/
void WaveNetDeviceExample::CreateWaveNodes (void)
{
  // 1. create the nodes
  nodes = NodeContainer ();
  nodes.Create (10);

  packetReceived = 0 ; 

  // 2. install mobility model in nodes

  MobilityHelper obu_mobility, rsu_mobility;

  // mobile obu positions
  obu_mobility.SetPositionAllocator("ns3::RandomRectanglePositionAllocator",
        "X",StringValue("ns3::UniformRandomVariable[Min=0.0|Max=500.0]"),
        "Y",StringValue("ns3::UniformRandomVariable[Min=0.0|Max=10.0]"));


  obu_mobility.SetMobilityModel("ns3::RandomWalk2dMobilityModel",
      "Bounds",  RectangleValue (Rectangle (0.0, 500.0, 0.0, 500.0)),
      "Distance",DoubleValue(1000.0),
      "Speed",StringValue("ns3::UniformRandomVariable[Min=25.0|Max=25.0]")); //mode distance, speed 40
  
  for(uint32_t i = 1 ; i < nodes.GetN();i++)
  {
	obu_mobility.Install (nodes.Get(i));
  }
  // fixed rsu position. 
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));
  rsu_mobility.SetPositionAllocator (positionAlloc);
  rsu_mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  rsu_mobility.Install (nodes.Get(0));
  
  
  // 3. install phy mac layers in each nodes/devics

  YansWifiChannelHelper waveChannel = YansWifiChannelHelper::Default ();
  waveChannel.SetPropagationDelay("ns3::ConstantSpeedPropagationDelayModel");

  waveChannel.AddPropagationLoss("ns3::NakagamiPropagationLossModel",
      "Distance1",DoubleValue(80.0),
      "Distance2",DoubleValue(300),
      "m0",DoubleValue(1.5),
      "m1",DoubleValue(0.75),
      "m2",DoubleValue(0));

  YansWavePhyHelper wavePhy =  YansWavePhyHelper::Default ();
  wavePhy.SetChannel (waveChannel.Create ());
  wavePhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11);
  QosWaveMacHelper waveMac = QosWaveMacHelper::Default ();
  WaveHelper waveHelper = WaveHelper::Default ();
  devices = waveHelper.Install (wavePhy, waveMac, nodes);

  // 5. cretae the callback functions.

  for (uint32_t i = 0; i != devices.GetN (); ++i)
  {
      Ptr<WaveNetDevice> device = DynamicCast<WaveNetDevice> (devices.Get (i));
      device->SetReceiveCallback (MakeCallback (&WaveNetDeviceExample::Receive, this));
      device->SetWaveVsaCallback (MakeCallback  (&WaveNetDeviceExample::ReceiveVsa, this));
  }

  // Tracing
  wavePhy.EnablePcap ("wave-simple-device", devices);
}

bool WaveNetDeviceExample::Receive (Ptr<NetDevice> dev, Ptr<const Packet> pkt, uint16_t mode, const Address &sender)
{
  packetReceived++;
  SeqTsHeader seqTs;
  pkt->PeekHeader (seqTs);
  std::cout << "receive a packet: " << std::endl
            << "  sequence = " << seqTs.GetSeq () << "," << std::endl
            << "  sendTime = " << seqTs.GetTs ().GetSeconds () << "s," << std::endl
            << "  recvTime = " << Now ().GetSeconds () << "s," << std::endl
            << "  protocol = 0x" << std::hex << mode << std::dec  << std::endl 
            << "  receiver = "  << dev->GetAddress() << std::endl
            << "  sender = " << sender << std::endl;
  
  std::cout<< "packet counter=" << packetReceived << std::endl;

  if(dev->GetAddress() == devices.Get(0)->GetAddress())
  {
    std::cout<< "packet received by node number 0"<< std::endl;
  }
  return true;
}

void WaveNetDeviceExample::SendOneWsmpPacket  (uint32_t channel, uint32_t seq)
{
  Ptr<WaveNetDevice>  sender = DynamicCast<WaveNetDevice> (devices.Get (0));
  Ptr<WaveNetDevice>  receiver = DynamicCast<WaveNetDevice> (devices.Get (1));
  const static uint16_t WSMP_PROT_NUMBER = 0x88DC;
  Mac48Address bssWildcard = Mac48Address::GetBroadcast ();

  const TxInfo txInfo = TxInfo (channel);
  Ptr<Packet> p  = Create<Packet> (100);
  SeqTsHeader seqTs;
  seqTs.SetSeq (seq);
  p->AddHeader (seqTs);
  sender->SendX  (p, bssWildcard, WSMP_PROT_NUMBER, txInfo);
}

void WaveNetDeviceExample::SendWsmpExample ()
{
  CreateWaveNodes ();
  Ptr<WaveNetDevice>  sender = DynamicCast<WaveNetDevice> (devices.Get (0));
  Ptr<WaveNetDevice>  receiver = DynamicCast<WaveNetDevice> (devices.Get (1));


  // Alternating access without immediate channel switch
  const SchInfo schInfo = SchInfo (SCH1, false, EXTENDED_ALTERNATING);
  Simulator::Schedule (Seconds (0.0), &WaveNetDevice::StartSch,sender,schInfo);
  // An important point is that the receiver should also be assigned channel
  // access for the same channel to receive packets.
  Simulator::Schedule (Seconds (0.0), &WaveNetDevice::StartSch, receiver, schInfo);

  // send WSMP packets
  // the first packet will be queued currently and be transmitted in next SCH interval
  Simulator::Schedule (Seconds (1.0), &WaveNetDeviceExample::SendOneWsmpPacket,  this, SCH1, 1);
  // the second packet will be queued currently and then be transmitted , because of in the CCH interval.
  Simulator::Schedule (Seconds (1.0), &WaveNetDeviceExample::SendOneWsmpPacket,  this, CCH, 2);
  // the third packet will be dropped because of no channel access for SCH2.
  Simulator::Schedule (Seconds (1.0), &WaveNetDeviceExample::SendOneWsmpPacket,  this, SCH2, 3);

  // release SCH access
  Simulator::Schedule (Seconds (2.0), &WaveNetDevice::StopSch, sender, SCH1);
  Simulator::Schedule (Seconds (2.0), &WaveNetDevice::StopSch, receiver, SCH1);
  // the fourth packet will be queued and be transmitted because of default CCH access assigned automatically.
  Simulator::Schedule (Seconds (3.0), &WaveNetDeviceExample::SendOneWsmpPacket,  this, CCH, 4);
  // the fifth packet will be dropped because of no SCH1 access assigned
  Simulator::Schedule (Seconds (3.0), &WaveNetDeviceExample::SendOneWsmpPacket,  this, SCH1, 5);

  Simulator::Stop (Seconds (5.0));
  Simulator::Run ();
  Simulator::Destroy ();
}
/**
* for IPv4 the paramers are ipv6 == false
**/
void WaveNetDeviceExample::SendIpPacket (uint32_t seq, bool ipv6)
{
  // send IPv4 packet or IPv6 packet
  const static uint16_t IPv4_PROT_NUMBER = 0x0800;
  const static uint16_t IPv6_PROT_NUMBER = 0x86DD;
  
  Ptr<WaveNetDevice>  receiver = DynamicCast<WaveNetDevice> (devices.Get (0));
  const Address dest = receiver->GetAddress ();
  
  for(uint32_t i = 1 ; i < devices.GetN();i++)
  {
	  Ptr<WaveNetDevice>  sender = DynamicCast<WaveNetDevice> (devices.Get (i));
	  uint16_t protocol = ipv6 ? IPv6_PROT_NUMBER : IPv4_PROT_NUMBER;
	  Ptr<Packet> p  = Create<Packet> (200);
	  SeqTsHeader seqTs;
	  seqTs.SetSeq (seq);
	  p->AddHeader (seqTs);
	  sender->Send (p, dest, protocol);
  }
}

void WaveNetDeviceExample::SendIpExample ()
{
  CreateWaveNodes ();
  const SchInfo schInfo = SchInfo (SCH1, false, EXTENDED_ALTERNATING);
  const TxProfile txProfile = TxProfile (SCH1);
  
  Ptr<WaveNetDevice>  receiver = DynamicCast<WaveNetDevice> (devices.Get (0));
  Simulator::Schedule (Seconds (0.0), &WaveNetDevice::StartSch, receiver, schInfo);
  
  for(uint32_t i = 1 ; i < devices.GetN();i++)
  {
	  Ptr<WaveNetDevice>  sender = DynamicCast<WaveNetDevice> (devices.Get (i));
	  Simulator::Schedule (Seconds (0.0), &WaveNetDevice::StartSch, sender, schInfo);
	  Simulator::Schedule (Seconds (2.0), &WaveNetDevice::RegisterTxProfile, sender, txProfile);
  }
  
  //schedule the packet sending 
  for(uint32_t i = 5 ; i < 3000 ; i++)
  {
    Simulator::Schedule (Seconds (i *1.0), &WaveNetDeviceExample::SendIpPacket, this, i, false);
  }

  Simulator::Stop (Seconds (300.0));
  Simulator::Run ();
  Simulator::Destroy ();
}

bool WaveNetDeviceExample::ReceiveVsa (Ptr<const Packet> pkt,const Address & address, uint32_t, uint32_t)
{
  std::cout << "receive a VSA management frame: recvTime = " << Now ().GetSeconds () << "s." << std::endl;
  return true;
}

void WaveNetDeviceExample::SendWsaExample ()
{
  CreateWaveNodes ();
  Ptr<WaveNetDevice>  sender = DynamicCast<WaveNetDevice> (devices.Get (0));
  Ptr<WaveNetDevice>  receiver = DynamicCast<WaveNetDevice> (devices.Get (1));

// Alternating access without immediate channel switch for sender and receiver
  const SchInfo schInfo = SchInfo (SCH1, false, EXTENDED_ALTERNATING);
  Simulator::Schedule (Seconds (0.0), &WaveNetDevice::StartSch, sender, schInfo);
  Simulator::Schedule (Seconds (0.0), &WaveNetDevice::StartSch, receiver, schInfo);

// the peer address of VSA is broadcast address, and the repeat rate
// of VsaInfo is 100 per 5s, the VSA frame will be sent repeatedly.
  Ptr<Packet> wsaPacket = Create<Packet> (100);
  Mac48Address dest = Mac48Address::GetBroadcast ();
  const VsaInfo vsaInfo = VsaInfo (dest, OrganizationIdentifier (), 0, wsaPacket, SCH1, 100, VSA_TRANSMIT_IN_BOTHI);
  Simulator::Schedule (Seconds (1.0), &WaveNetDevice::StartVsa, sender, vsaInfo);
  Simulator::Schedule (Seconds (3.0), &WaveNetDevice::StopVsa, sender, SCH1);

// release alternating access
  Simulator::Schedule (Seconds (4.0), &WaveNetDevice::StopSch, sender, SCH1);
  Simulator::Schedule (Seconds (4.0), &WaveNetDevice::StopSch, receiver, SCH1);

// these WSA packets cannot be transmitted because of no channel access assigned
  Simulator::Schedule (Seconds (5.0), &WaveNetDevice::StartVsa, sender, vsaInfo);
  Simulator::Schedule (Seconds (6.0), &WaveNetDevice::StopVsa, sender, SCH1);

  Simulator::Stop (Seconds (300.0));
  Simulator::Run ();
  Simulator::Destroy ();
}

int main (int argc, char *argv[])
{
    srand (time(NULL));

  std::string traceFile;
  std::string logFile;

  int    nodeNum;
  double duration;
  
  bool verbose = false;
  bool tracing = true;
  int numPackets = 2;


  //uint32_t sinkNode = 0;


  NS_LOG_COMPONENT_DEFINE ("wave-simple-device");
  
  LogComponentEnable ("Ns2MobilityHelper",LOG_LEVEL_DEBUG);

  CommandLine cmd;
  cmd.AddValue ("traceFile", "Ns2 movement trace file", traceFile);
  cmd.AddValue ("nodeNum", "Number of nodes", nodeNum);
  cmd.AddValue ("duration", "Duration of Simulation", duration);
  cmd.AddValue ("logFile", "Log file", logFile);
  cmd.AddValue ("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("tracing", "turn on ascii and pcap tracing", tracing);
  cmd.AddValue ("numPackets", "number of packets generated", numPackets);



  // this is for testing
  //std::string phyMode ("DsssRate1Mbps");
  //std::cout<<"phyMode is DsssRate1Mbps"<<phyMode<<std::endl;
  //double distance = 300; // m
  //uint32_t packetSize = 200; // bytes
  //uint32_t numNodes = 25;  // by default, 5x5
  //uint32_t sourceNode = 24;
  //double interval = 1.0; // seconds 1


  //uint32_t sinkNode = 0;
  //cmd.AddValue ("phyMode", "Wifi Phy mode", phyMode);
  //cmd.AddValue ("distance", "distance (m)", distance);
  //cmd.AddValue ("packetSize", "size of application packet sent", packetSize);
  //cmd.AddValue ("numPackets", "number of packets generated", numPackets);
  //cmd.AddValue ("interval", "interval (seconds) between packets", interval);
  //cmd.AddValue ("numNodes", "number of nodes", numNodes);
  //cmd.AddValue ("sinkNode", "Receiver node number", sinkNode);
  //cmd.AddValue ("sourceNode", "Sender node number", sourceNode);

  cmd.Parse (argc, argv);

  /*
  std::string phyMode ("DsssRate1Mbps");
  std::cout<<"phyMode is DsssRate1Mbps"<<phyMode<<std::endl;
  double distance = 300; // m
  std::cout<<"distance is "<<distance<<std::endl;
  uint32_t packetSize = 200; // bytes
  std::cout<<"Packet size"<<packetSize<<std::endl;
  uint32_t numPackets = 100;
  std::cout<<"numPackets "<<numPackets<<std::endl;
  uint32_t numNodes = 25;  // by default, 5x5
  uint32_t sourceNode = 24;
  double interval = 1.0; // seconds 1
  bool verbose = false;
  bool tracing = true;

  uint32_t sinkNode = 0;

  /// command line setup 

  CommandLine cmd;
  cmd.AddValue ("phyMode", "Wifi Phy mode", phyMode);
  cmd.AddValue ("distance", "distance (m)", distance);
  cmd.AddValue ("packetSize", "size of application packet sent", packetSize);
  cmd.AddValue ("numPackets", "number of packets generated", numPackets);
  cmd.AddValue ("interval", "interval (seconds) between packets", interval);
  cmd.AddValue ("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("tracing", "turn on ascii and pcap tracing", tracing);
  cmd.AddValue ("numNodes", "number of nodes", numNodes);
  cmd.AddValue ("sinkNode", "Receiver node number", sinkNode);
  cmd.AddValue ("sourceNode", "Sender node number", sourceNode);
  cmd.Parse (argc, argv);

  */


  WaveNetDeviceExample example;
  std::cout << "run WAVE WSMP routing service case:" << std::endl;
  example.SendIpExample ();
  std::cout << "run WAVE IP routing service case:" << std::endl;
  //example.SendIpExample ();
  std::cout << "run WAVE WSA routing service case:" << std::endl;
  //example.SendWsaExample ();
  return 0;
}
